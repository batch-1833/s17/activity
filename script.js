console.log("Hello World!");


// 3. Create an addStudent() function that will accept a name of the student and add it to the student array.


let students = [];

function addStudent(studentName){
	students.push(studentName.toLowerCase());
	console.log(studentName + " was added to the array list")
	console.log(studentName)
}



// 4. Create a countStudents() function that will print the total number of students in the array.
function countStudents() {
	console.log("A total of "+students.length+ " students in the array.");
}





// 5. Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.

function printStudents(){

	console.log(students.sort());

	students.forEach(function(student){

		console.log(student);
	})
}

/*6. Create a findStudent() function that will do the following:
Search for a student name when a keyword is given (filter method).
If one match is found print the message studentName is an enrollee.
If multiple matches are found print the message studentNames are enrollees.
If no match is found print the message studentName is not an enrollee.
The keyword given should not be case sensitive.
*/

function findStudent(studentName){

	let outputStudents = students.filter(function(student){
		return student.toLowerCase().includes(studentName);
	});

	if (outputStudents.length === 1) {
		console.log(outputStudents + " is an enrollee")
	}
	else if (outputStudents.length > 1){
		console.log(outputStudents + " are enrollees");
	}
	else {
		console.log("No match found"+ "," +outputStudents+ " The student is not an enrollee.");
	}
}




